package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Service;

import com.example.demo.model.StudentModel;
import com.example.demo.repository.StudentRepository;
import com.github.javafaker.Faker;

@Service
public class StudentServiceImple implements StudentService {

	StudentRepository studentRepository;
	
	public StudentServiceImple(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public List<StudentModel> findAll() {
		List<StudentModel> students = studentRepository.findAll();
		return students;
	}

	@Override
	public StudentModel findOne(int id) {
		return studentRepository.findOne(id);
	}

	@Override
	public boolean save(StudentModel student) {
		return studentRepository.save(student);
	}

	@Override
	public boolean remove(int id) {
	
		return studentRepository.remove(id);
	}

	@Override
	public boolean update(StudentModel student) {
		
		return studentRepository.update(student);
	}
	
	@Override
    public List<StudentModel> findStudentByPage(int limit, int pageNumber) {
        int maxpage=(int)findAll().size()/limit;
        if(findAll().size()%limit!=0)maxpage++;
        if(limit<5)limit=5;
        if(pageNumber<1)pageNumber=1;
        pageNumber--;
        if(pageNumber>maxpage)pageNumber=maxpage;
        return studentRepository.findStudentByPage(limit, pageNumber*limit);
    }
 

}
