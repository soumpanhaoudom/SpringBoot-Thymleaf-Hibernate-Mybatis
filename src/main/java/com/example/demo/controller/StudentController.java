package com.example.demo.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.StudentModel;
import com.example.demo.service.StudentService;

import groovy.lang.Binding;

@Controller
public class StudentController {
	
	private StudentService studentService;
	
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	
//	Homepage
	
	@GetMapping ("/student")
	public String homePage(ModelMap model){
		List<StudentModel> students = studentService.findAll();
		model.addAttribute("students", students);
		return "student";
	}
	
//	View Student
	
	@GetMapping ("/student/view")
	public String viewStudent(ModelMap model, @RequestParam("id") int id){
		StudentModel students =  studentService.findOne(id);
		model.addAttribute("students", students);
		return "studentDetail";
	}
	
//	Remove Student
	
	@GetMapping ("/student/remove/{id}")
	public String removeStudent(ModelMap model, @PathVariable("id") int id){
		if(studentService.remove(id))
			System.out.println("Remove successfull!");
		else
			System.out.println("Remove failed");
		return "redirect:/student";
	}
	
//	Add Student
	
	@GetMapping ("/student/add")
	public String addStudent(ModelMap model){
		model.addAttribute("student",new StudentModel() );
		return "addStudent";
	}
	
//	Save Student
	
	@PostMapping ("/student/save")
	public String saveStudent(StudentModel student, BindingResult result){
		if (result.hasErrors()){
			System.out.println(result.getFieldError().getDefaultMessage());
			return "redirect:/student/add";
		}
		if(studentService.save(student))
			System.out.println("success!");
		System.out.println(student);
		return "redirect:/student";
	}
	
//	Edit Student
	
	@GetMapping ("/student/edit/{id}")
	public String editStudent(@PathVariable("id") int id, ModelMap model){
		model.addAttribute("student",studentService.findOne(id));
		return "updateStudent";
	}
	
//	Update Student
	
	@PostMapping ("/student/update")
	public String updateStudent(StudentModel student, BindingResult result){
		if(result.hasErrors()){
			return "redirect:/student/edit" + student.getId();
		}
		if(studentService.update(student))
			System.out.println("success!");
		return "redirect:/student";
	}
	
//	Show Students by pagenation
	
	@GetMapping("/student/page")
    public String getViews(@PathParam("pageNumber") int pageNumber,ModelMap model)
    {
		int limit = 10;
        int maxpage=(int)studentService.findAll().size()/limit;
        if(studentService.findAll().size()%limit!=0)maxpage++;
        if(pageNumber>maxpage)pageNumber=maxpage;
        model.addAttribute("students",studentService.findStudentByPage(limit,pageNumber));
        model.addAttribute("maxpage",maxpage);
        model.addAttribute("pageNumber",pageNumber);
        return "student";
    }
 
}
