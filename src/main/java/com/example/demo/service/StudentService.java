package com.example.demo.service;

import java.util.List;

import com.example.demo.model.StudentModel;

public interface StudentService {
	
	List<StudentModel> findAll();
	
	StudentModel findOne(int id);
	
	boolean save(StudentModel student);
	
	boolean remove(int id);
	
	boolean update(StudentModel student );
	
	 List<StudentModel> findStudentByPage(int limit,int pageNumber);

}
