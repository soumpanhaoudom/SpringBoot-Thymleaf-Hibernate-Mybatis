//package com.example.demo.repository;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Locale;
//
//import org.springframework.stereotype.Repository;
//
//import com.example.demo.model.StudentModel;
//import com.github.javafaker.Faker;
//
//@Repository
//public class StudentRepositoryImple implements StudentRepository{
//
//	private List<StudentModel> students;
//	
//	public StudentRepositoryImple() {
//		Faker faker = new Faker(Locale.ENGLISH);
//		students = new ArrayList<>();
//		for(int i=0; i<20; i++){
//			StudentModel studentModel = new StudentModel();
//			studentModel.setId(i+1);
//			studentModel.setName(faker.name().fullName());
//			studentModel.setDescription(faker.address().cityName());
//			studentModel.setThumbnail("http://www.wallpaperstop.com/wallpapers/flower-wallpapers/flower-imag-336x210-0111.jpg");
//			students.add(studentModel);
//		}
//	}
//	
//	@Override
//	public List<StudentModel> findAll() {
//		
//		return students;
//	}
//
//	@Override
//	public StudentModel findOne(int id) {
//		for(StudentModel student: students){
//			if(student.getId()==id)
//				return student;
//		}
//		return null;
//	}
//
//	@Override
//	public boolean save(StudentModel student) {
//		return students.add(student);
//	}
//
//	@Override
//	public boolean remove(int id) {
//		for(StudentModel student : students ){
//			if(student.getId()==id){
//				students.remove(student);
//				return true;
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public boolean update(StudentModel student) {
//		for(int i=0; i<students.size();i++){
//			if(student.getId() == students.get(i).getId()){
//				students.set(i, student);
//				return true;
//			}
//		}
//		return false;
//	}
//	
//}
