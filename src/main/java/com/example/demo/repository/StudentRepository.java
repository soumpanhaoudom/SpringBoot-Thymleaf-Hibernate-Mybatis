package com.example.demo.repository;

import java.util.List;


import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.StudentModel;
import com.example.demo.service.StudentService;

import org.apache.ibatis.annotations.*;

@Repository
public interface StudentRepository {
	
	
	@Select("SELECT * FROM tbl_student") 
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "name", column = "name"),
		@Result(property = "description", column = "description"),
		@Result(property = "thumbnail", column = "thumnail"),
	})
	List<StudentModel> findAll();
	
	@Select("SELECT * FROM tbl_student WHERE id = #{id}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "name", column = "name"),
		@Result(property = "description", column = "description"),
		@Result(property = "thumbnail", column = "thumnail"),
	})
	StudentModel findOne(int id);
	
	@Insert("INSERT INTO tbl_student(name, description, thumnail) VALUES(#{name}, #{description}, #{thumbnail})")
	boolean save(StudentModel student);
	
	@Delete("DELETE FROM tbl_student WHERE id=#{id}")
	boolean remove(int id);
	
	@Update("UPDATE tbl_student SET name=#{name}, description=#{description}, thumnail= #{thumbanil} WHERE id=#{id}")
	boolean update(StudentModel student);
	
//	show student by pagenation
	
	@Select("SELECT * FROM tbl_student LIMIT #{limit} OFFSET #{offset}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "name", column = "name"),
		@Result(property = "description", column = "description"),
		@Result(property = "thumbnail", column = "thumnail"),
	})
	List<StudentModel> findStudentByPage(@Param("limit") int limit, @Param("offset") int offset);
}
