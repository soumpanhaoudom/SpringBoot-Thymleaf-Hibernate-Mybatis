package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanhaoudomApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanhaoudomApplication.class, args);
	}
}
